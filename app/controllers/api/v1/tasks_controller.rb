class Api::V1::TasksController < ApplicationController
  skip_before_filter :verify_authenticity_token
  
  def index
    if params[:username]
      @tasks = Task.where({:username => params[:username].to_s})
    else
      @tasks = Task.all
    end
    render
  end

  def show
    @task = Task.find params[:id]
    if @task.present?
      render
    end
  end

  def create 
    @task = Task.new(permit_params)
    if @task.save
      render
    else
      respond_to_errors(@task)
    end
  end

  def update 
    @task = Task.find params[:id]
    if @task.update_attributes(permit_params)
      render
    else
      respond_to_errors(@task)
    end
  end

  def destroy
    @task = Task.find params[:id]
    if @task.destroy
      render
    else
      respond_to_errors(@task)
    end
  end

  def permit_params
    params.require(:task).permit(:description, :complete, :username)
  end

end
