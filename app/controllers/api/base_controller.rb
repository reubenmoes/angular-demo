class Api::BaseController < ApplicationController
  skip_before_action :verify_authenticity_token
  def respond_to_errors(object)
    render json: {errors: object.errors}, status: :unprocessable_entity
  end
end
