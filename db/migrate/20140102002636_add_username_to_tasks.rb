class AddUsernameToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :username, :string
  end
end
